-module(lib_misc).
-export([for/3, qsort/1, pythag/1, perms/1, 
	odds_and_evens1/1, odds_and_evens2/1, my_tuple_to_list/1,
	my_time_function/1, my_count_characters/1]).

for(Max,Max,F)->[F(Max)];
for(I,Max,F)->[F(I)|for(I+1,Max,F)].

qsort([]) -> [];
qsort([Pivot|T]) ->
	qsort([X || X <- T, X < Pivot])
	++ [Pivot] ++
	qsort([X || X <- T, X >= Pivot]).

pythag(N)	->
	[ {A,B,C} ||
		A <- lists:seq(1, N),
		B <- lists:seq(1, N),
		C <- lists:seq(1, N),
		A+B+C =< N,
		A*A+B*B =:= C*C
	].

perms([])	-> [[]];
perms(L)  -> [[H|T] || H <- L, T <- perms(L--[H])].

odds_and_evens1(L) ->
	Odds  = [X || X <- L, (X rem 2) =:= 1],
	Evens = [X || X <- L, (X rem 2) =:= 0],
	{ Odds, Evens }.

odds_and_evens2(L) ->
	odds_and_evens_acc(L, [], []).

odds_and_evens_acc([H|T], Odds, Events)	->
	case (H rem 2) of
		1 -> odds_and_evens_acc(T, [H|Odds], Events);
		0 -> odds_and_evens_acc(T, Odds, [H|Events])
	end;

odds_and_evens_acc([], Odds, Events) ->
	{lists:reverse(Odds), lists:reverse(Events)}.


my_tuple_to_list({}) -> [];
my_tuple_to_list(T) -> my_tuple_to_list2(tuple_size(T), T, []).

my_tuple_to_list2(0, _, Acc) -> Acc;

my_tuple_to_list2(N, T, Acc) -> 
	my_tuple_to_list2(N - 1, T, [element(N, T)|Acc]).

my_time_function(F) ->
	Begin = erlang:timestamp(),
	F(),
	End 	= erlang:timestamp(),
	MegaSecs  = element(1, End) - element(1, Begin),
	Secs      = element(2, End) - element(2, Begin),
	MicroSecs = element(3, End) - element(3, Begin),
	{MegaSecs, Secs, MicroSecs}.


my_count_characters(Str) -> my_count_characters(Str, #{}).

my_count_characters([H|R], Acc) ->
	ChCount = maps:get(H, Acc, 0), % get current chapter count or 0
	my_count_characters(R, Acc#{H => ChCount + 1});

my_count_characters([], Acc) -> Acc.	



% count_characters(Str) ->
% 	count_characters(Str, #{}).

% count_characters([H|T], #{ H => N } = X) ->
% 	count_characters(T, X#{ H := N + 1 });

% count_characters([H|T], X) ->
% 	count_characters(T, #{ H => 1 }, X);

% count_characters([], X) ->
% 	X.